//
//  AddImageCell.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import UIKit

class AddImageCell: UICollectionViewCell {

    var avatarTapped: ( ()->() )?
    
    @IBOutlet weak var avatarView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarView.isUserInteractionEnabled = true
        avatarView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
        
    }
    
    func tapped() {
    
        if let avatarTapped = avatarTapped {
            avatarTapped()
        }
    }

}
