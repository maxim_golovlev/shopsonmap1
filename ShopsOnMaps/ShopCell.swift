//
//  ShopCell.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import UIKit

class ShopCell: UITableViewCell {

    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var title: UILabel!
    
}
