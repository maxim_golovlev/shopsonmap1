//
//  Shop+CoreDataProperties.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import Foundation
import CoreData


extension Shop {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Shop> {
        return NSFetchRequest<Shop>(entityName: "Shop")
    }

    @NSManaged public var images: NSData?
    @NSManaged public var title: String?
    @NSManaged public var lat: Float
    @NSManaged public var lng: Float

}
