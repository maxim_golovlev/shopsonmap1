import Foundation
import MBProgressHUD
import UIKit

protocol Loadable {
    func startLoading()
    func stopLoading()
}

extension Loadable where Self: UIViewController {
    func startLoading() {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: false)
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: false)
        }        
    }
}

extension Loadable where Self: UICollectionViewCell {
    func startLoading() {
        MBProgressHUD.showAdded(to: self, animated: false)
    }
    
    func stopLoading() {
        MBProgressHUD.hide(for: self, animated: false)
    }
}

