//
//  CreateShopController.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import UIKit
import CoreData

class CreateShopController: UIViewController, Alertable, Loadable {

    @IBOutlet weak var containderView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var addedImages = [UIImage]()
    var containerViewController: AdressController?
    
    let imageCellId = "imageCellId"
    let addImageCellId = "addImageCellId"
    let containerSegueName = "StaticTableViewSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGesture))
        self.collectionView.addGestureRecognizer(longPressGesture)
        
        collectionView.register(UINib.init(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: imageCellId)
        collectionView.register(UINib.init(nibName: "AddImageCell", bundle: nil), forCellWithReuseIdentifier: addImageCellId)

    }
    
    func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
            collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case UIGestureRecognizerState.changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case UIGestureRecognizerState.ended:
            collectionView.endInteractiveMovement()
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
    
    func addNewImage() {
        
        let picker = UIImagePickerController()
        picker.delegate = self

        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
        
    }
    @IBAction func saveAndQuit(_ sender: Any) {
        
        startLoading()
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.currentContext {
            
            let imageData = prepearImages()
            
            var success = false
       
            let adress = self.getCurrentAdress()
            
            GoogleAPI.sharedInstance.fetchLocation(from: adress, completion: { (location) in
                
                if let location = location {
                    
                    context.performAndWait {
                        if let newShop = NSEntityDescription.insertNewObject(forEntityName: "Shop", into: context) as? Shop {
                            
                            newShop.images = imageData as NSData
                    
                            newShop.lat = location.lat
                            newShop.lng = location.lng
                            
                            newShop.title = self.containerViewController?.shopNameTextField.text
                            
                            try? context.save()
                        }
                    }
                    success = true

                }
                
                self.stopLoading()
                
                let message = success ? "Локация успешно добавлена" : "Ошибка определения адреса"
                
                self.showAlert(title: "", message: message, handler: { (action) in
                    
                    if success {
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            })
        }
    }
    
    func getCurrentAdress() -> Adress {
        
        let vc = containerViewController
        var number: Int16? = nil
        
        if let n = vc?.houseNumber.text {
            number = Int16(n)
        }
        
        let adress = Adress(houseNumber: number,
                            street: vc?.streetTextField.text,
                            city: vc?.cityTextField.text,
                            country: vc?.countryTextField.text)
        return adress
    }
    
    func prepearImages() -> Data {
        
        var dataArray = [Data]()
        
        for img in addedImages{
            if let data : Data = UIImageJPEGRepresentation(img, 0.2) {
            dataArray.append(data)
            }
        }
        
        let coreDataObject = NSKeyedArchiver.archivedData(withRootObject: dataArray);
        
        return coreDataObject
    }
    
    @IBAction func cancelAndDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == containerSegueName {
            containerViewController = segue.destination as? AdressController
        }
    }
}

extension CreateShopController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1 + addedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let index = indexPath.item
        
        switch index {
        case 1 + addedImages.count - 1:
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addImageCellId, for: indexPath) as? AddImageCell {
                
                cell.avatarTapped = {
                    self.addNewImage()
                }
                return cell
            }
            
        default:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellId, for: indexPath) as? ImageCell {
                
                cell.image.image = addedImages[index]
                
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
}


extension CreateShopController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 160, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        
    }
}

extension CreateShopController: UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            addedImages.append(chosenImage)
            collectionView.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         dismiss(animated: true, completion: nil)
    }
    
}
