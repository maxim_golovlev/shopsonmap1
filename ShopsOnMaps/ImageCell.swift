//
//  ImageCell.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
