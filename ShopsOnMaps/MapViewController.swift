//
//  MapViewController.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData

class MapViewController: UIViewController, Alertable {

    var shops = [Shop]()
    var locationManager = CLLocationManager()
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.isMyLocationEnabled = true

        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    
        fetchMarkers()
    }
    
    func fetchMarkers() {
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.currentContext {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Shop")
            
            do {
                if let shops = try context.fetch(fetchRequest) as? [Shop] {
                    self.shops = shops
                    renderMarkers()
                }
                
            } catch {
                self.showAlert(title: "Ошибка", message: "Не удалось загрузить маркеры")
            }
        }
    }
    
    func renderMarkers() {
        
        for shop in shops {
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(shop.lat), longitude: CLLocationDegrees(shop.lng))
            marker.title = shop.title
            marker.map = mapView
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude:(location?.coordinate.longitude)!, zoom:10)
        mapView.animate(to: camera)
        
        self.locationManager.stopUpdatingLocation()
    }
    
}
