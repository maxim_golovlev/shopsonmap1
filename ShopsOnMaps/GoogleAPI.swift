//
//  GoogleAPI.swift
//  ShopsOnMaps
//
//  Created by qwe on 09/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import Foundation

struct Location {
    var lat: Float = 0
    var lng: Float = 0
}

struct Adress {
    var houseNumber: Int16?
    var street: String?
    var city: String?
    var country: String?
}

class GoogleAPI {
    
    static let sharedInstance = GoogleAPI()
    
    
    func fetchLocation(from adress: Adress, completion: @escaping (Location?) -> ()) {
        
        guard let houseNumber = adress.houseNumber, let street = adress.street, let city = adress.city, let country = adress.country else { completion(nil); return }
        
        if let urlString = "http://maps.google.com/maps/api/geocode/json?address=\(houseNumber)\(street)\(city)\(country)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL.init(string: urlString) {
            
            URLSession.shared.dataTask(with: url, completionHandler: { (data, responce, error) in
                
                guard error == nil else {
                    print(error!)
                    completion(nil); return
                }
                guard let data = data else {
                    print("Data is empty")
                    completion(nil); return
                }
                
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else { completion(nil); return }
                
                if let results = (json as? [String: Any])?["results"] as? [[String: Any]] {
                    
                    if let geometry = results.first( where: { $0.keys.contains("geometry") })? ["geometry"] as? [String: Any] {
                        
                        if let loc = geometry["location"] as? [String: Any] {
                            
                            let location = Location(lat: Float(loc["lat"] as! Float), lng: Float(loc["lng"] as! Float))
                            
                            completion(location)
                        }
                    }
                    
                } else {
                    completion(nil)
                }
                
            }).resume()
            
        }
    }
}
