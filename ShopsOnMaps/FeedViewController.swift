//
//  ViewController.swift
//  ShopsOnMaps
//
//  Created by qwe on 08/08/2017.
//  Copyright © 2017 Maxim Golovlev. All rights reserved.
//

import UIKit
import CoreData

class FeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var shops = [Shop]()
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib.init(nibName: "ShopCell", bundle: nil), forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.currentContext {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Shop")
            
            do {
                if let shops = try context.fetch(fetchRequest) as? [Shop] {
                    self.shops = shops
                    tableView.reloadData()
                }
                
            } catch {
                print(error)
            }
            
        }
    }
}

extension FeedViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? ShopCell {
            
            let shop = shops[indexPath.row]
            
            cell.title.text = shop.title
            
            if let imagesArray = NSKeyedUnarchiver.unarchiveObject(with: shop.images! as Data) as? [Data] {
                
                if imagesArray.count > 0, let firstImage = UIImage.init(data: imagesArray[0])  {
                    cell.logoView.image = firstImage
                }
            }
            
            return cell
        }

        return UITableViewCell()
    }
    
}

extension FeedViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

